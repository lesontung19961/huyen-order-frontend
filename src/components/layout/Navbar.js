import React from "react";
import {Link} from "react-router-dom";
import SignedInLinks from "./SignedInLinks";
import SignedOutLinks from "./SignedOutLinks";
import {connect} from "react-redux";
import Logo from "../../images/logo512.svg";

import {SideNav} from "react-materialize";

const Navbar = (props) => {
  return (
    <>
      <div className="navbar-fixed">
        <nav className="nav-wrapper grey darken-3">
          <div className="container">
            <Link to="/" className="brand-logo left">
              <img src={Logo} alt="logo" />
              Orderly
            </Link>

            <div className="hide-on-med-and-down right">{props.authUser ? <SignedInLinks /> : <SignedOutLinks />}</div>

            <div className="show-on-medium-and-down right">
              <a href="#/" data-target="sidenav" className="sidenav-trigger">
                <i className="material-icons">menu</i>
              </a>
            </div>
          </div>
        </nav>
      </div>

      <SideNav
        id="sidenav"
        className="hide-on-med-and-up"
        options={{
          draggable: true,
        }}>
        {props.authUser ? <SignedInLinks /> : <SignedOutLinks />}
      </SideNav>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    authUser: state.auth.authUser,
  };
};

export default connect(mapStateToProps)(Navbar);
