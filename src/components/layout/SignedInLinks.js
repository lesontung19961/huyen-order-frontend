import React from "react";
import {NavLink} from "react-router-dom";

const SignedInLinks = () => {
  return (
    <>
      <ul>
        <li>
          <NavLink to="/">Đơn hàng</NavLink>
        </li>
        <li>
          <NavLink to="/customer">Khách hàng</NavLink>
        </li>
        <li>
          <NavLink to="/statistic">Thống kê</NavLink>
        </li>
        <li>
          <NavLink to="/logout">Đăng xuất</NavLink>
        </li>
        {/* <li>
          <NavLink to="/" className="btn btn-floating pink lighten-1">
            NN
          </NavLink>
        </li> */}
      </ul>
    </>
  );
};

export default SignedInLinks;
