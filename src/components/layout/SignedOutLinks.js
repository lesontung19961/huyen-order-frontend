import React from "react";
import {NavLink} from "react-router-dom";

const SignedOutLinks = () => {
  return (
    <div>
      <ul>
        <li>
          <NavLink to="/signup">Đăng ký</NavLink>
        </li>
        <li>
          <NavLink to="/signin">Đăng nhập</NavLink>
        </li>
      </ul>
    </div>
  );
};

export default SignedOutLinks;
