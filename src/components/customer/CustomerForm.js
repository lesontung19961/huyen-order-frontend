import React, {Component} from "react";

import {connect} from "react-redux";

export class CustomerForm extends Component {
  render() {
    const {handleSubmit, handleChange, customer} = this.props;

    return (
      <form className="white col" onSubmit={handleSubmit} ref={(ref) => (this.myDiv = ref)}>
        <div className="row">
          <div className="col m6">
            <div className="input-field">
              <input type="text" id="name" value={customer.name || ""} onChange={handleChange} />
              <label htmlFor="name" className={customer.id ? "active" : ""}>
                Tên Khách hàng
              </label>
            </div>
          </div>

          <div className="col m6">
            <div className="input-field">
              <input type="text" id="phone" value={customer.phone || ""} onChange={handleChange} />
              <label htmlFor="phone" className={customer.phone ? "active" : ""}>
                Số điện thoại
              </label>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col m6">
            <div className="input-field">
              <input type="text" id="link_facebook" value={customer.link_facebook || ""} onChange={handleChange} />
              <label htmlFor="link_facebook" className={customer.link_facebook ? "active" : ""}>
                Link Facebook
              </label>
            </div>
          </div>

          <div className="col m6">
            <div className="input-field">
              <textarea id="note" className="materialize-textarea" value={customer.note || ""} onChange={handleChange}></textarea>
              <label htmlFor="note" className={customer.id ? "active" : ""}>
                Ghi chú
              </label>
            </div>
          </div>
        </div>

        <div className="input-field right-align">
          <button className="modal-close waves-effect waves-green btn">Cập nhật</button>
        </div>
      </form>
    );
  }
}

export default connect()(CustomerForm);
