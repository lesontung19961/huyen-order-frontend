import React, {Component} from "react";
import {connect} from "react-redux";
import {updateCustomer} from "../../store/actions/customerActions";
import CustomerForm from "./CustomerForm";

const defaultState = {
  name: "",
  phone: "",
  link_facebook: "",
  note: "",
};

class UpdateCustomer extends Component {
  state = defaultState;

  componentDidUpdate = (previousProps) => {
    if (previousProps.currentFields !== this.props.currentFields) {
      this.setState(this.props.currentFields);
    }
  };

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleSubmit = async (e) => {
    e.preventDefault();

    await this.props.setLoadingState(true);

    this.props.updateCustomer(this.state).then(() => {
      this.props.setLoadingState(false);
    });

    this.setState(defaultState);
  };
  render() {
    return (
      <div>
        <CustomerForm handleChange={this.handleChange} customer={this.state} handleSubmit={this.handleSubmit} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentFields: state.customer.curentFields,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCustomer: (customer) => dispatch(updateCustomer(customer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateCustomer);
