import React, {Component} from "react";
import {fetchCustomers} from "../../store/actions/customerActions";
import CustomerTable from "./CustomerTable";
import CustomerCreate from "./CustomerCreate";
import CustomerUpdate from "./CustomerUpdate";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import PreLoader from "../helpers/PreLoader";

import {Modal} from "react-materialize";

const {innerWidth} = window;
const btnFixedClassDesktop = "btn-floating btn-large modal-trigger waves-effect";
const btnFixedClassMobile = "btn-floating btn-large modal-trigger grey darken-3 z-depth-0";

export class CustomerDashboard extends Component {
  state = {
    isLoading: false,
  };

  setLoadingState = (bool) => {
    this.setState({isLoading: bool});
  };

  componentDidMount = () => {
    this.props.fetchCustomers();
  };

  render() {
    const authUserData = localStorage.getItem("authUserData");
    const jsonAuthUserData = JSON.parse(authUserData);
    if (!jsonAuthUserData) return <Redirect to="/signin" />;

    return (
      <>
        <div className="container">
          <h1>Khách hàng</h1>
        </div>

        <CustomerTable customers={this.props.customers} setLoadingState={this.setLoadingState} />

        <Modal
          actions={null}
          header="Tạo Khách hàng mới"
          className="white modal__no-footer"
          id="addCustomerModal"
          open={false}
          fixedFooter={false}
          options={{
            preventScrolling: false,
          }}
          trigger={
            <div className="btn-fixed">
              <a className={innerWidth > 1024 ? btnFixedClassDesktop : btnFixedClassMobile} href="#addCustomerModal">
                <i className="material-icons">add</i>
              </a>
            </div>
          }>
          <CustomerCreate setLoadingState={this.setLoadingState} />
        </Modal>

        <Modal
          actions={null}
          header="Chỉnh sửa Khách hàng"
          className="white modal__no-footer"
          id="updateCustomerModal"
          open={false}
          fixedFooter={false}
          options={{
            preventScrolling: false,
          }}>
          <CustomerUpdate setLoadingState={this.setLoadingState} />
        </Modal>

        {this.state.isLoading && <PreLoader />}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    customers: state.customer.customers,
  };
};

const mapDispathToProps = (dispath) => {
  return {
    fetchCustomers: () => dispath(fetchCustomers()),
  };
};

export default connect(mapStateToProps, mapDispathToProps)(CustomerDashboard);
