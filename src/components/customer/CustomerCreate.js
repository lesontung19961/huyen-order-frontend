import React, {Component} from "react";
import {connect} from "react-redux";
import {createCustomer} from "../../store/actions/customerActions";
import CustomerForm from "./CustomerForm";

const defaultState = {
  name: "",
  phone: "",
  link_facebook: "",
  note: "",
};

export class CustomerCreate extends Component {
  state = defaultState;

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleSubmit = async (e) => {
    e.preventDefault();

    await this.props.setLoadingState(true);

    this.props.createCustomer(this.state).then(() => {
      this.props.setLoadingState(false);
    });
    this.setState(defaultState);
  };

  render() {
    return (
      <div>
        <CustomerForm handleChange={this.handleChange} customer={this.state} handleSubmit={this.handleSubmit} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createCustomer: (customer) => dispatch(createCustomer(customer)),
  };
};

export default connect(null, mapDispatchToProps)(CustomerCreate);
