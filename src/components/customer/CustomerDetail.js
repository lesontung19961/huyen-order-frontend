import React, {Component} from "react";
import {fetchCustomerDetail} from "../../store/actions/customerActions";
import OrderTable from "../order/OrderTable";
import OrderSumarry from "../order/OrderSummary";
import PreLoader from "../helpers/PreLoader";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";

export class CustomerDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
  }

  componentDidMount = async () => {
    const {fetchCustomerDetail, match} = this.props;
    await fetchCustomerDetail(match.params.id);
    this.setState({isLoading: false});
  };

  render() {
    const authUserData = localStorage.getItem("authUserData");
    const jsonAuthUserData = JSON.parse(authUserData);
    if (!jsonAuthUserData) return <Redirect to="/signin" />;

    const {customer} = this.props;
    return !customer ? (
      <PreLoader />
    ) : (
      <div>
        <div className="container">
          <div className="customer-detail__info">
            <h4 className="grey-text text-darken-3">{customer?.name}</h4>
            <p className="grey-text text-darken-1">
              <b>Số điện thoại: </b>
              <span>
                <a href={`tel:${customer.phone}`}>{customer.phone}</a>
              </span>
            </p>
            <p className="grey-text text-darken-1">
              <b>Link Facebook: </b>
              <span>
                <a href={`${customer.link_facebook}`}>{customer.link_facebook}</a>
              </span>
            </p>
            <p className="grey-text text-darken-1">
              <b>Ghi chú: </b>
              <span>{customer.note}</span>
            </p>
          </div>
        </div>

        {customer.orders.length ? (
          <div className="customer-detail__table">
            <OrderTable orders={customer.orders} hideCustomerCollumn={true} />

            <OrderSumarry orders={customer.orders} />
          </div>
        ) : (
          <div className="container">
            <p className="grey-text center-align">Khách hàng này chưa mua sản phẩm nào</p>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    customer: state.customer.customerDetail,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCustomerDetail: (id) => dispatch(fetchCustomerDetail(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomerDetail);
