import React from "react";
import {deleteCustomer, setCurrentFieldCustomer} from "../../store/actions/customerActions";
import {NavLink} from "react-router-dom";
import PreLoader from "../helpers/PreLoader";

import {connect} from "react-redux";

function CustomerTable({customers, deleteCustomer, setCurrentFieldCustomer, setLoadingState}) {
  const deleteCustomerPressed = async (item) => {
    await setLoadingState(true);
    deleteCustomer(item).then(() => {
      setLoadingState(false);
    });
  };

  const renderRow = customers?.length ? (
    customers
    .sort((a, b) => {
      return b.orders.length - a.orders.length
    })
    .map((item) => {
      const badgeColor = item.orders.length ? "green" : "red";
      return (
        <tr key={item.id}>
          <td>{item.id}</td>
          <td className="modal-trigger" data-target="updateCustomerModal" onClick={() => setCurrentFieldCustomer(item)}>
            <span className="red-text text-darken-2">{item.name}</span>
          </td>
          <td>
            <a href={`tel:${item.phone}`}>{item.phone}</a>
          </td>
          <td className="collumn-longtext">
            <a href={item.link_facebook} target="_blank" rel="noopener noreferrer">
              {item.link_facebook}
            </a>
          </td>
          <td>
            <NavLink to={`customer/${item.id}`}>
              <span className={`badge new ${badgeColor}`} data-badge-caption="">
                {item.orders.length} Đơn hàng
              </span>
            </NavLink>
          </td>
          <td className="collumn-longtext">{item.note}</td>
          <td>
            <a
              onClick={() => deleteCustomerPressed(item)}
              className="waves-effect waves-light red toast-deleted btn-floating btn-small">
              <i className="material-icons left">delete</i>
            </a>
          </td>
        </tr>
      );
    })
  ) : (
    <tr></tr>
  );

  return !customers ? (
    <PreLoader />
  ) : (
    <div className="table-responsive">
      <table className="striped highlight table-customers">
        <thead>
          <tr>
            <th>ID</th>
            <th>Tên Khách hàng</th>
            <th>Số điện thoại </th>
            <th>Link Facebook</th>
            <th>Lịch sử mua hàng</th>
            <th>Ghi chú</th>
            <th>Xóa</th>
          </tr>
        </thead>
        <tbody>{renderRow}</tbody>
      </table>
    </div>
  );
}

const mapDispathToProps = (dispath) => {
  return {
    setCurrentFieldCustomer: (customer) => dispath(setCurrentFieldCustomer(customer)),
    deleteCustomer: (customer) => dispath(deleteCustomer(customer)),
  };
};

export default connect(null, mapDispathToProps)(CustomerTable);
