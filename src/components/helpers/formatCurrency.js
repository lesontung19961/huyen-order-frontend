const formatCurrency = (money) => {
  return parseFloat(money)
    .toFixed(0)
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
};

export default formatCurrency;
