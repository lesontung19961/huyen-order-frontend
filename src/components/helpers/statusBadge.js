import {statusArray} from "../../config/main";
import React from "react";

const StatusBadge = ({status, left, quantity}) => {
  let color = "yellow";
  switch (status) {
    case statusArray[0]:
      color = "yellow darken-4";
      break;

    case statusArray[1]:
      color = "blue";
      break;

    case statusArray[2]:
      color = "red darken-2";
      break;

    case statusArray[3]:
      color = "green";
      break;
    default:
      color = "green";
  }

  return (
    <span className={`badge new ${color} ${left ? "left" : ""} ${!quantity ? "empty" : ""}`} data-badge-caption="">
      {status}
    </span>
  );
};

export default StatusBadge;
