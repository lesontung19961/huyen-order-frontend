import React, {Component} from "react";
import axios from "axios";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import {authLogout} from "../../store/actions/authActions";

class LogOut extends Component {
  componentDidMount = () => {
    localStorage.removeItem("authUserData");
    delete axios.defaults.headers.common["Authorization"];
    this.props.authLogout();
  };

  render() {
    return <Redirect to="/" />;
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    authLogout: () => dispatch(authLogout()),
  };
};

export default connect(null, mapDispatchToProps)(LogOut);
