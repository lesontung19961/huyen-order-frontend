import React, {Component} from "react";
import axios from "axios";
import {connect} from "react-redux";
import {authLogin} from "../../store/actions/authActions";
import {Redirect} from "react-router-dom";
import PreLoader from "../helpers/PreLoader";

class SignIn extends Component {
  state = {
    identifier: "",
    password: "",
    isLoading: false,
  };

  setBearerToken = (jwt) => {
    axios.defaults.headers.common["Authorization"] = `Bearer ${jwt}`;
  };

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleSubmit = async (e) => {
    const {history, authLogin} = this.props;

    e.preventDefault();
    await this.setState({isLoading: true});

    authLogin(this.state)
      .then((userData) => {
        this.setState({isLoading: false});
        localStorage.setItem("authUserData", JSON.stringify(userData));
        this.setBearerToken(userData.jwt);
        history.push("/");
      })
      .catch(() => {
        alert("Bạn đã nhập sai tên tài khoản hoặc mật khẩu");
      });
  };

  render() {
    const authUserData = localStorage.getItem("authUserData");
    const jsonAuthUserData = JSON.parse(authUserData);
    if (jsonAuthUserData) return <Redirect to="/" />;

    return (
      <>
        <div className="container">
          <form className="white" onSubmit={this.handleSubmit}>
            <h5 className="grey-text text-darken-3">Đăng nhập</h5>
            <div className="input-field">
              <label htmlFor="identifier">Tài khoản</label>
              <input type="text" id="identifier" onChange={this.handleChange} />
            </div>
            <div className="input-field">
              <label htmlFor="password">Mật khẩu</label>
              <input type="password" id="password" onChange={this.handleChange} />
            </div>
            <div className="input-field">
              <button className="btn pink lighten-1 z-depth-0">Đăng nhập</button>
            </div>
          </form>
        </div>
        {this.state.isLoading && <PreLoader />}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    authUser: state.auth.authUser,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    authLogin: (body) => dispatch(authLogin(body)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
