import React, {Component} from "react";
import axios from "axios";
import {Redirect} from "react-router-dom";
import {connect} from "react-redux";
import {authSignUp} from "../../store/actions/authActions";
import PreLoader from "../helpers/PreLoader";

class SignUp extends Component {
  state = {
    username: "",
    password: "",
    passwordRepeat: "",
    email: "",
    isLoading: false,
  };
  setBearerToken = (jwt) => {
    axios.defaults.headers.common["Authorization"] = `Bearer ${jwt}`;
  };

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleSubmit = async (e) => {
    const {history, authSignUp} = this.props;
    e.preventDefault();

    await this.setState({isLoading: true});

    if (this.state.password === this.state.passwordRepeat) {
      authSignUp(this.state)
        .then(async (userData) => {
          await alert("Đăng ký thành công, chúng tôi sẽ chuyển bạn vào trong ứng dụng!");
          localStorage.setItem("authUserData", JSON.stringify(userData));
          this.setBearerToken(userData.jwt);
          history.push("/");
        })
        .catch((error) => {
          alert("Tên tài khoản hoặc email đã bị trùng, vui lòng nhập lại!");
        })
        .then(() => {
          this.setState({isLoading: false});
        });
    } else {
      alert("Mật khẩu không khớp");
    }
  };
  render() {
    const authUserData = localStorage.getItem("authUserData");
    const jsonAuthUserData = JSON.parse(authUserData);
    if (jsonAuthUserData) return <Redirect to="/" />;

    return (
      <>
        <div className="container">
          <form className="white" onSubmit={this.handleSubmit}>
            <h5 className="grey-text text-darken-3">Đăng ký</h5>
            <div className="input-field">
              <label htmlFor="username">Tên tài khoản</label>
              <input type="text" id="username" onChange={this.handleChange} />
            </div>
            <div className="input-field">
              <label htmlFor="email">Email</label>
              <input type="text" id="email" onChange={this.handleChange} />
            </div>
            <div className="input-field">
              <label htmlFor="password">mật khẩu</label>
              <input type="password" id="password" onChange={this.handleChange} />
            </div>
            <div className="input-field">
              <label htmlFor="passwordRepeat">Nhập lại mật khẩu</label>
              <input type="password" id="passwordRepeat" onChange={this.handleChange} />
            </div>
            <div className="input-field">
              <button className="btn pink lighten-1 z-depth-0">Đăng ký</button>
            </div>
          </form>
        </div>
        {this.state.isLoading && <PreLoader />}
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    authUser: state.auth.authUser,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    authSignUp: (body) => dispatch(authSignUp(body)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
