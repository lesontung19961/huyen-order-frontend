import React, {Component} from "react";
import {connect} from "react-redux";
import {updateOrder} from "../../store/actions/orderActions";
import {createCustomer} from "../../store/actions/customerActions";
import OrderForm from "./OrderForm";

const defaultState = {
  select_customer_method: false,
  new_customer: {
    name: "",
    phone: "",
    link_facebook: "",
    note: "",
  },
};

class UpdateOrder extends Component {
  state = defaultState;

  componentDidUpdate = (previousProps) => {
    if (previousProps.currentFields !== this.props.currentFields) {
      this.setState(this.props.currentFields);
    }
  };

  fakeRender = () => {
    this.setState(defaultState);
  };

  handleChangeSwitch = (e) => {
    this.setState({
      [e.target.id]: e.target.checked,
    });
  };

  handleChangeNewCustomer = (e) => {
    this.setState({
      new_customer: {
        ...this.state.new_customer,
        [e.target.id]: e.target.value,
      },
    });
  };

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleChangeDate = (value) => {
    this.setState({
      date: value,
    });
  };

  handleSubmit = async (e) => {
    e.preventDefault();

    await this.props.setLoadingState(true);

    // Nếu có dữ liệu new_customer thì tạo mới customer rồi gán luôn customer này vào order
    if (this.state.new_customer !== defaultState.new_customer) {
      await this.props.createCustomer(this.state.new_customer).then((data) => {
        if (data.id) this.setState({customer: data.id}); // Lấy dữ liệu customer vừa tạo gán vào order
      });
    }

    this.props.updateOrder(this.state).then(() => {
      this.props.setLoadingState(false);
    });
    this.setState(defaultState);
  };
  render() {
    return (
      <div>
        <OrderForm
          handleChange={this.handleChange}
          handleChangeNewCustomer={this.handleChangeNewCustomer}
          handleChangeSwitch={this.handleChangeSwitch}
          order={this.state}
          handleSubmit={this.handleSubmit}
          handleChangeDate={this.handleChangeDate}
          fakeRender={this.fakeRender}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentFields: state.order.curentFields,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateOrder: (order) => dispatch(updateOrder(order)),
    createCustomer: (customer) => dispatch(createCustomer(customer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateOrder);
