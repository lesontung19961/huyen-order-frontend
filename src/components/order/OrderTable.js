import React, {useState, useEffect} from "react";
import formatCurrency from "../helpers/formatCurrency";
import StatusBadge from "../helpers/statusBadge";
import {setCurrentFieldOrder, deleteOrder} from "../../store/actions/orderActions";
import {connect} from "react-redux";
import PreLoader from "../helpers/PreLoader";
import {Link} from "react-router-dom";

function OrderTable({orders, deleteOrder, setCurrentFieldOrder, hideCustomerCollumn = false, setLoadingState}) {
  const [isLoading, setIsLoading] = useState(true);
  const [orderState, setOrderState] = useState([]);
  const [sortConfig, setSortConfig] = useState({key: "date", ascending: true});

  useEffect(() => {
    setOrderState(orders);
  });

  useEffect(() => {
    if (orderState?.length) setIsLoading(false);
  }, [orderState]);

  useEffect(() => {
    setActiveLabel();
    const newOrder = sortConfig.ascending
      ? orderState.sort((a, b) => (a[sortConfig.key] > b[sortConfig.key] ? 1 : -1))
      : orderState.sort((a, b) => (a[sortConfig.key] < b[sortConfig.key] ? 1 : -1));

    setOrderState([...newOrder]);
  }, [sortConfig]);

  const deleteOrderPressed = async (item) => {
    await setLoadingState(true);
    deleteOrder(item).then(() => {
      setLoadingState(false);
    });
  };

  const sort = (e) => {
    const {sort} = e.target.dataset;
    const newSortConfig = {
      key: sort,
      ascending: sortConfig && sortConfig.key === sort ? !sortConfig.ascending : true,
    };
    setSortConfig(newSortConfig);
  };

  const removeActiveLabel = () => {
    let tableHeads = document.querySelectorAll("th[data-sort]");
    tableHeads.forEach((item) => {
      item.dataset.active = false;
      delete item.dataset.sortOrder;
    });
  };

  const setActiveLabel = () => {
    const sortElement = document.querySelector(`[data-sort="${sortConfig.key}"]`);
    removeActiveLabel();
    sortElement.dataset.sortOrder = sortConfig.ascending ? "asc" : "desc";
    sortElement.dataset.active = true;
  };

  const renderRow = orderState?.length ? (
    orderState.map((item) => {
      return (
        <tr key={item.id}>
          <td>{item.id}</td>
          {hideCustomerCollumn === false && (
            <td>
              {item.customer?.name && (
                <>
                  <a
                    href={item.customer?.link_facebook || "#/"}
                    target={item.customer?.link_facebook ? "_blank" : ""}
                    className="customer__link-facebook">
                    <span
                      className={`new badge ${item.customer?.link_facebook ? "blue darken-3" : "grey"} `}
                      data-badge-caption="">
                      {item.customer.name}
                    </span>
                  </a>
                  <Link to={`customer/${item.customer.id}`}>
                    <span className={`new badge blue-grey`} data-badge-caption="">
                      Chi tiết
                    </span>
                  </Link>
                </>
              )}
            </td>
          )}
          <td className="modal-trigger" data-target="updateOrderModal" onClick={() => setCurrentFieldOrder(item)}>
            <span className="red-text text-darken-2">{item.name}</span>
          </td>
          <td>{formatCurrency(item.money_thu)}</td>
          <td>{formatCurrency(item.money_lai)}</td>
          <td>{item.date}</td>
          <td>
            <StatusBadge status={item.status} />
          </td>
          <td className="collumn-longtext">
            <a href={item.link_product} target="_blank" rel="noopener noreferrer">
              {item.link_product}
            </a>
          </td>
          <td className="collumn-longtext">{item.code}</td>
          <td className="collumn-longtext">{item.note}</td>
          {hideCustomerCollumn === false && (
            <td>
              <a
                onClick={() => deleteOrderPressed(item)}
                className="waves-effect waves-light red toast-deleted btn-floating btn-small">
                <i className="material-icons left">delete</i>
              </a>
            </td>
          )}
        </tr>
      );
    })
  ) : (
    <tr></tr>
  );

  return (
    <div className="table-responsive">
      <table className="striped highlight table-orders ">
        <thead>
          <tr>
            <th>ID</th>
            {hideCustomerCollumn === false && <th>Khách hàng</th>}
            <th data-sort="name" onClick={sort}>
              Tên Order
            </th>
            <th data-sort="money_thu" onClick={sort}>
              Tiền thu
            </th>
            <th data-sort="money_lai" onClick={sort}>
              Tiền Lãi
            </th>
            <th data-sort="date" onClick={sort}>
              Ngày
            </th>
            <th data-sort="status" onClick={sort}>
              Trạng thái
            </th>
            <th>Link Sản phẩm</th>
            <th>Mã Đơn hàng</th>
            <th>Ghi chú</th>
            {hideCustomerCollumn === false && <th>Xóa</th>}
          </tr>
        </thead>
        <tbody>{renderRow}</tbody>
      </table>

      {isLoading && <PreLoader />}
    </div>
  );
}

const mapDispathToProps = (dispath) => {
  return {
    setCurrentFieldOrder: (order) => dispath(setCurrentFieldOrder(order)),
    deleteOrder: (order) => dispath(deleteOrder(order)),
  };
};

export default connect(null, mapDispathToProps)(OrderTable);
