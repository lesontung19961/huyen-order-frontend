import React, {Component} from "react";
import {statusArray} from "../../config/main";
import {DatePicker, Select} from "react-materialize";
import M from "materialize-css";
import {connect} from "react-redux";

export class OrderForm extends Component {
  componentDidMount() {
    M.AutoInit();
  }

  componentDidUpdate = (previousProps) => {
    if (previousProps.order.id !== this.props.order.id) {
      if (this.props.fakeRender) this.props.fakeRender();
    }
  };

  render() {
    const renderStatusOptions = () => {
      return statusArray.map((item, index) => (
        <option key={index} value={item}>
          {item}
        </option>
      ));
    };

    const renderCustomerOptions = () => {
      if (this.props.customers) {
        const newCustomers = [{id: "", name: "Chọn"}, ...this.props.customers.sort((a, b) => a.name.localeCompare(b.name))];
        return newCustomers.map((item, index) => {
          return (
            <option key={index} disabled={index === 0 ? true : false} value={item.id}>
              {item.name}
            </option>
          );
        });
      }
    };

    const {
      handleSubmit,
      handleChange,
      order,
      handleChangeDate,
      handleChangeNewCustomer,
      handleChangeSwitch,
    } = this.props;

    return (
      <form className="white col" onSubmit={handleSubmit} ref={(ref) => (this.myDiv = ref)}>
        <div className="row">
          <div className="col m4 s6">
            <div className="input-field">
              <input type="text" id="name" value={order.name || ""} onChange={handleChange} />
              <label htmlFor="name" className={order.id ? "active" : ""}>
                Tên Order
              </label>
            </div>
          </div>
          <div className="col m4 s6">
            <div className="input-field">
              <input type="text" id="link_product" value={order.link_product || ""} onChange={handleChange} />
              <label htmlFor="link_product" className={order.id ? "active" : ""}>
                Link Sản phẩm
              </label>
            </div>
          </div>
          <div className="col m4 s6">
            <div className="input-field">
              <input type="text" id="code" value={order.code || ""} onChange={handleChange} />
              <label htmlFor="code" className={order.id ? "active" : ""}>
                Mã đơn hàng
              </label>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col m4 s6">
            <div className="input-field">
              <input type="number" id="money_thu" value={order.money_thu || ""} step={1000} onChange={handleChange} />
              <label htmlFor="money_thu" className={order.id ? "active" : ""}>
                Tiền thu
              </label>
            </div>
          </div>
          <div className="col m4 s6">
            <div className="input-field">
              <input type="number" id="money_lai" value={order.money_lai || ""} step={1000} onChange={handleChange} />
              <label htmlFor="money_lai" className={order.id ? "active" : ""}>
                Tiền Lãi
              </label>
            </div>
          </div>
          <div className="col m4 s6">
            <Select
              className="removeClassCol"
              id="status"
              onChange={handleChange}
              label="Chọn trạng thái"
              value={order.status || ""}>
              {renderStatusOptions()}
            </Select>
          </div>
        </div>

        <div className="row">
          <div className="col m6 s6">
            <DatePicker id="date" value={order.date || ""} onChange={handleChangeDate} className="removeClassCol">
              <label htmlFor="date" className="active">
                Ngày
              </label>
            </DatePicker>
          </div>
          <div className="col m6 s6">
            <div className="input-field">
              <textarea
                id="note"
                className="materialize-textarea"
                value={order.note || ""}
                onChange={handleChange}></textarea>
              <label htmlFor="note" className={order.id ? "active" : ""}>
                Ghi chú
              </label>
            </div>
          </div>
        </div>

        <div className="order-form__customer">
          <div className="switch">
            <label>
              Khách cũ
              <input
                type="checkbox"
                id="select_customer_method"
                checked={order.select_customer_method}
                onChange={handleChangeSwitch}
              />
              <span className="lever"></span>
              Khách mới
            </label>
          </div>

          {order.select_customer_method === true ? (
            <div className="row">
              <div className="col m6 s6">
                <div className="input-field">
                  <input
                    type="text"
                    id="name"
                    value={order.new_customer.name || ""}
                    onChange={handleChangeNewCustomer}
                  />
                  <label htmlFor="name" className={order.id ? "active" : ""}>
                    Tên Khách hàng
                  </label>
                </div>
              </div>

              <div className="col m6 s6">
                <div className="input-field">
                  <input
                    type="text"
                    id="phone"
                    value={order.new_customer.phone || ""}
                    onChange={handleChangeNewCustomer}
                  />
                  <label htmlFor="phone" className={order.id ? "active" : ""}>
                    Số điện thoại
                  </label>
                </div>
              </div>

              <div className="col m6 s6">
                <div className="input-field">
                  <input
                    type="text"
                    id="link_facebook"
                    value={order.new_customer.link_facebook || ""}
                    onChange={handleChangeNewCustomer}
                  />
                  <label htmlFor="link_facebook" className={order.id ? "active" : ""}>
                    Link Facebook
                  </label>
                </div>
              </div>

              <div className="col m6 s6">
                <div className="input-field">
                  <input
                    type="text"
                    id="note"
                    value={order.new_customer.note || ""}
                    onChange={handleChangeNewCustomer}
                  />
                  <label htmlFor="note" className={order.id ? "active" : ""}>
                    Ghi chú
                  </label>
                </div>
              </div>
            </div>
          ) : (
            <Select id="customer" onChange={handleChange} value={order.customer?.id || ""}>
              {renderCustomerOptions()}
            </Select>
          )}
        </div>

        <div className="input-field right-align">
          <button className="modal-close waves-effect waves-green btn">Cập nhật</button>
        </div>
      </form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    customers: state.customer.customers,
  };
};

export default connect(mapStateToProps)(OrderForm);
