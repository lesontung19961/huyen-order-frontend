import React, {useState} from "react";
import formatCurrency from "../helpers/formatCurrency";
import StatusBadge from "../helpers/statusBadge";
import {statusArray} from "../../config/main";
import {connect} from "react-redux";
import {setFilterStatusOrder} from "../../store/actions/orderActions";

function OrderSummary({orders, setFilterStatusOrder}) {
  const [filterActive, setFilterActive] = useState(false);

  const filterHandler = (status) => {
    setFilterStatusOrder(status);
    status ? setFilterActive(true) : setFilterActive(false);
  };

  const total_money_thu = () => {
    let total = 0;
    orders &&
      orders.map((item) => {
        return (total = total + parseInt(item.money_thu));
      });
    return total;
  };

  const total_money_lai = () => {
    let total = 0;
    orders &&
      orders.map((item) => {
        return (total = total + parseInt(item.money_lai));
      });
    return total;
  };

  const statusSummary = () => {
    return statusArray.map((arrayItem) => {
      const filteredItems = orders.filter((item) => item.status === arrayItem);

      return (
        <li className="order-summary__item right" key={arrayItem} onClick={() => filterHandler(arrayItem)}>
          <StatusBadge status={arrayItem} quantity={filteredItems.length} left={true} /> : {filteredItems.length}
        </li>
      );
    });
  };

  return (
    <div className="order-summary__wrapper">
      <div className="order-summary">
        <ul className="order-summary__left">
          <li className="order-summary__item">
            Tổng đơn:
            <span className={`badge new blue`} data-badge-caption="">
              {orders && orders.length}
            </span>
          </li>
          <li className="order-summary__item">
            Tổng tiền thu:
            <span className={`badge new green`} data-badge-caption="">
              {formatCurrency(total_money_thu())}
            </span>
          </li>
          <li className="order-summary__item">
            Tổng tiền lãi:
            <span className={`badge new green`} data-badge-caption="">
              {formatCurrency(total_money_lai())}
            </span>
          </li>
        </ul>
        <ul className="order-summary__right">
          <li className="order-summary__item right">Bộ lọc:</li>
          {filterActive && (
            <li className="order-summary__item right" onClick={() => filterHandler(null)}>
              <a href="#/">Đặt lại</a>
            </li>
          )}
          {orders && statusSummary()}
        </ul>
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    setFilterStatusOrder: (status) => dispatch(setFilterStatusOrder(status)),
  };
};

export default connect(null, mapDispatchToProps)(OrderSummary);
