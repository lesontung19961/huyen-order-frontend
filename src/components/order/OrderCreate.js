import React, {Component} from "react";
import {connect} from "react-redux";
import {createOrder} from "../../store/actions/orderActions";
import {createCustomer} from "../../store/actions/customerActions";
import OrderForm from "./OrderForm";
import {statusArray} from "../../config/main";

const defaultState = {
  name: "",
  money_lai: 0,
  money_thu: 0,
  status: statusArray[0],
  link_product: "",
  date: new Date(),
  code: "",
  note: "",
  select_customer_method: false,
  new_customer: {
    name: "",
    phone: "",
    link_facebook: "",
    note: "",
  },
};

class CreateOrder extends Component {
  state = defaultState;

  handleChangeSwitch = (e) => {
    this.setState({
      [e.target.id]: e.target.checked,
    });
  };

  handleChangeNewCustomer = (e) => {
    this.setState({
      new_customer: {
        ...this.state.new_customer,
        [e.target.id]: e.target.value,
      },
    });
  };

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleChangeDate = (value) => {
    this.setState({
      date: value,
    });
  };

  handleSubmit = async (e) => {
    e.preventDefault();

    await this.props.setLoadingState(true);

    // Nếu có dữ liệu new_customer thì tạo mới customer rồi gán luôn customer này vào order
    if (this.state.new_customer !== defaultState.new_customer) {
      await this.props.createCustomer(this.state.new_customer).then((data) => {
        if (data.id) this.setState({customer: data.id}); // Lấy dữ liệu customer vừa tạo gán vào order
      });
    }

    this.props.createOrder(this.state).then(() => {
      this.props.setLoadingState(false);
    });
    this.setState(defaultState);
  };
  render() {
    return (
      <div className="">
        <OrderForm
          handleChange={this.handleChange}
          handleChangeNewCustomer={this.handleChangeNewCustomer}
          handleChangeSwitch={this.handleChangeSwitch}
          order={this.state}
          handleSubmit={this.handleSubmit}
          handleChangeDate={this.handleChangeDate}
          // fakeRender={this.fakeRender}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createOrder: (order) => dispatch(createOrder(order)),
    createCustomer: (customer) => dispatch(createCustomer(customer)),
  };
};

export default connect(null, mapDispatchToProps)(CreateOrder);
