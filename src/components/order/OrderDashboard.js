import React, {Component} from "react";
import {fetchOrders} from "../../store/actions/orderActions";
import {fetchCustomers} from "../../store/actions/customerActions";
import OrderTable from "./OrderTable";
import OrderCreate from "./OrderCreate";
import OrderUpdate from "./OrderUpdate";
import OrderSummary from "./OrderSummary";
import {connect} from "react-redux";
import PreLoader from "../helpers/PreLoader";

import {Redirect} from "react-router-dom";

import {Modal} from "react-materialize";

const {innerWidth} = window;
const btnFixedClassDesktop = "btn-floating btn-large modal-trigger waves-effect";
const btnFixedClassMobile = "btn-floating btn-large modal-trigger grey darken-3 z-depth-0";

export class OrderDashboard extends Component {
  state = {
    isLoading: false,
  };

  setLoadingState = (bool) => {
    this.setState({isLoading: bool});
  };

  componentDidMount = () => {
    this.props.fetchOrders();
    this.props.fetchCustomers();
    this.removeClassCol();
  };

  removeClassCol = () => {
    var removeClassCol = document.querySelectorAll(".removeClassCol");
    removeClassCol.forEach((element) => {
      element.closest(".input-field.col").classList.remove("col");
    });
  };

  render() {
    const authUserData = localStorage.getItem("authUserData");
    const jsonAuthUserData = JSON.parse(authUserData);
    if (!jsonAuthUserData) return <Redirect to="/signin" />;

    return (
      <>
        <div className="container">
          <h1>Đơn hàng</h1>
        </div>

        <OrderTable orders={this.props.orders} setLoadingState={this.setLoadingState} />

        <OrderSummary orders={this.props.orders} />

        <Modal
          actions={null}
          header="Tạo Order mới"
          className="white modal__no-footer"
          id="addOrderModal"
          open={false}
          fixedFooter={false}
          options={{
            preventScrolling: false,
          }}
          trigger={
            <div className="btn-fixed">
              <a className={innerWidth > 1024 ? btnFixedClassDesktop : btnFixedClassMobile} href="#addOrderModal">
                <i className="material-icons">add</i>
              </a>
            </div>
          }>
          <OrderCreate setLoadingState={this.setLoadingState} />
        </Modal>

        <Modal
          actions={null}
          header="Chỉnh sửa Order"
          className="white modal__no-footer"
          id="updateOrderModal"
          open={false}
          fixedFooter={false}
          options={{
            preventScrolling: false,
          }}>
          <OrderUpdate setLoadingState={this.setLoadingState} />
        </Modal>

        {this.state.isLoading && <PreLoader />}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  const {statusFilter, orders} = state.order;
  return {
    orders: statusFilter ? orders.filter((item) => item.status === statusFilter) : orders,
    statusFilter: statusFilter,
    newCustomer: state.customer.newCustomer,
  };
};

const mapDispathToProps = (dispath) => {
  return {
    fetchOrders: () => dispath(fetchOrders()),
    fetchCustomers: () => dispath(fetchCustomers()),
  };
};

export default connect(mapStateToProps, mapDispathToProps)(OrderDashboard);
