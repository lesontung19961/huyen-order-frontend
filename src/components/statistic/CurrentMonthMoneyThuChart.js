import React, {Component} from "react";
import formatCurrency from "../helpers/formatCurrency";
import Chart from "react-apexcharts";

export class CurrentMonthMoneyThuChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: {
        title: {
          text: "Tiền thu",
          style: {
            fontFamily: "Segoe UI",
            fontWeight: "600",
            fontSize: "16px",
          },
        },
        chart: {toolbar: {show: false}},
        subtitle: {
          align: "right",
          floating: true,
          offsetY: 0,
        },
        dataLabels: {
          enabled: true,
          formatter: (value) => formatCurrency(value),
        },
        tooltip: {
          y: {
            formatter: (value) => formatCurrency(value),
          },
        },
        colors: ["#00e396"],
        xaxis: {
          type: "datetime",
          categories: [],
          labels: {
            format: "dd/MM/yyyy",
          },
        },
      },
      series: [],
    };
  }

  getAllDates = () => {
    const {orders, selectedMonth} = this.props;
    return [
      ...new Set(
        orders
          .filter(
            (item) =>
              new Date(item.date).getMonth() === new Date(selectedMonth).getMonth() &&
              new Date(item.date).getFullYear() === new Date(selectedMonth).getFullYear()
          )
          .map((item) => item.date)
          .sort()
      ),
    ];
  };

  setMonthSeriesData = () => {
    const {orders} = this.props;
    const allDates = this.getAllDates();

    const seriesData = [
      {
        name: "Tiền thu",
        data: allDates.map((item) => {
          let total = 0;
          orders.forEach((order) => {
            if (item === order.date) total = total + parseInt(order.money_thu);
          });
          return total;
        }),
      },
    ];

    this.setState({
      series: seriesData,
      options: {
        ...this.state.options,
        subtitle: {
          text: `Tổng: ${formatCurrency(seriesData[0].data.reduce((a, b) => a + b, 0))}`,
        },
      },
    });
  };

  setMonthCatData = () => {
    const allDates = this.getAllDates();

    this.setState({
      options: {
        ...this.state.options,
        xaxis: {
          ...this.state.options.xaxis,
          categories: allDates,
        },
      },
    });
  };

  componentDidUpdate = async (previousProps, previousState) => {
    if (this.props.selectedMonth !== previousProps.selectedMonth) {
      await this.setMonthCatData();
      this.setMonthSeriesData();
    }
  };

  componentDidMount = async () => {
    await this.setMonthCatData();
    this.setMonthSeriesData();
  };

  render() {
    return (
      <div id="line-2" className="current-month-money-thu-chart chart--box">
        <Chart options={this.state.options} series={this.state.series} type="area" height="350" width="100%" />
      </div>
    );
  }
}

export default CurrentMonthMoneyThuChart;
