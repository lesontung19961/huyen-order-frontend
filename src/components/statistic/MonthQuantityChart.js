import React, {Component} from "react";
import {connect} from "react-redux";
import {statusArray} from "../../config/main";
import Chart from "react-apexcharts";

export class MonthQuantityChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: {
        title: {
          text: "Lượng đơn hàng tháng",
          style: {
            fontFamily: "Segoe UI",
            fontWeight: "600",
            fontSize: "16px",
          },
        },
        chart: {
          stacked: true,
        },
        colors: ["#feb019", "#008ffb", "#ff4560", "#00e396", "#775DD0"],
        xaxis: {
          type: "datetime",
          categories: [],
          labels: {
            formatter: function (val) {
              const date = new Date(val);
              return `T${date.getMonth() + 1}, ${date.getFullYear()}`;
            },
          },
        },
        yaxis: [],
        fill: {
          opacity: 1,
        },
      },
      series: [],
    };
  }

  getAllMonths = () => {
    const {orders} = this.props;
    return [
      ...new Set(
        orders
          .map((item) => new Date(item.date))
          .sort((a, b) => a - b)
          .map((fullDate) => {
            const month = fullDate.getMonth() + 1;
            return fullDate.getFullYear() + "-" + month;
          })
      ),
    ];
  };

  setMonthSeriesData = () => {
    const {orders} = this.props;
    const allMonths = this.getAllMonths();

    const seriesDataColumn = statusArray.map((status) => {
      return {
        name: status,
        type: "column",
        data: allMonths.map((month) => {
          let count = 0;
          orders.forEach((order) => {
            if (
              new Date(order.date).getMonth() === new Date(month).getMonth() &&
              new Date(order.date).getFullYear() === new Date(month).getFullYear() &&
              status === order.status
            )
              count = count + 1;
          });
          return count;
        }),
      };
    });

    const seriesDataLine = {
      name: "Tiền lãi",
      type: "line",
      data: allMonths.map((month) => {
        return orders
          .filter(
            (order) =>
              new Date(order.date).getMonth() === new Date(month).getMonth() && new Date(order.date).getFullYear() === new Date(month).getFullYear()
          )
          .map((order) => order.money_lai)
          .reduce((a, b) => parseInt(a) + parseInt(b), 0);
      }),
    };

    this.setState({
      series: [...seriesDataColumn, seriesDataLine],
    });
  };

  setMonthCatData = () => {
    const allMonths = this.getAllMonths();

    this.setState({
      options: {
        ...this.state.options,
        xaxis: {
          ...this.state.options.xaxis,
          categories: allMonths,
          label: allMonths,
        },
      },
    });
  };

  setYAxisData = () => {
    const yAxis1 = statusArray.map((item, index) => {
      return {
        seriesName: statusArray[0],
        show: index === 0 ? true : false,
        title: {
          text: "Lượng đơn",
          style: {
            fontFamily: "Segoe UI",
            color: "#008FFB",
            fontWeight: "600",
          },
        },
      };
    });

    const yAxis2 = {
      seriesName: "Tiền lãi",
      opposite: true,
      axisTicks: {
        show: true,
      },
      title: {
        text: "Tiền lãi",
        style: {
          fontFamily: "Segoe UI",
          color: "#775DD0",
          fontWeight: "600",
        },
      },
    };

    this.setState({
      options: {
        ...this.state.options,
        yaxis: [...yAxis1, yAxis2],
      },
    });
  };

  componentDidMount = async () => {
    await this.setMonthCatData();
    this.setMonthSeriesData();
    this.setYAxisData();
  };

  render() {
    return (
      <div className="month-quantity-chart chart--box">
        <Chart options={this.state.options} series={this.state.series} type="line" height="350" width="100%" />
      </div>
    );
  }
}

export default MonthQuantityChart;
