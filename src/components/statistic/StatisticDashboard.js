import React, {Component} from "react";
import {fetchOrders} from "../../store/actions/orderActions";
import {connect} from "react-redux";
import {Select} from "react-materialize";
import PreLoader from "../helpers/PreLoader";
import MonthQuantityChart from "./MonthQuantityChart";
import CurrentMonthMoneyLaiChart from "./CurrentMonthMoneyLaiChart";
import CurrentMonthMoneyThuChart from "./CurrentMonthMoneyThuChart";

const defaultMonth = new Date().getFullYear() + "-" + (new Date().getMonth() + 1);

class StatisticDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedMonth: defaultMonth,
    };
  }

  renderMonths = () => {
    const {orders} = this.props;
    const allMonths = [
      ...new Set(
        orders
          .map((item) => new Date(item.date))
          .sort((a, b) => a - b)
          .map((date) => date.getFullYear() + "-" + (date.getMonth() + 1))
      ),
    ];

    return allMonths.map((item, index) => (
      <option key={index} value={item}>
        {new Date(item).getMonth() + 1 + "/" + new Date(item).getFullYear()}
      </option>
    ));
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  componentDidMount = () => {
    this.props.fetchOrders();
  };

  render() {
    const {orders} = this.props;

    return !orders ? (
      <PreLoader />
    ) : (
      <div className="chart--dashboard">
        <div className="container">
          <div className="inline-form">
            <h3 className="chart__title label">Thống kê tháng</h3>
            <Select
              name="selectedMonth"
              className="statistic-month-select"
              onChange={this.handleChange}
              value={this.state.selectedMonth}>
              {this.renderMonths()}
            </Select>
          </div>
          <div className="row">
            <div id="month-charts">
              <div className="col m6 s12">
                <CurrentMonthMoneyLaiChart orders={orders} selectedMonth={this.state.selectedMonth} />
              </div>
              <div className="col m6 s12">
                <CurrentMonthMoneyThuChart orders={orders} selectedMonth={this.state.selectedMonth} />
              </div>
            </div>
            <div className="col m12 s12">
              <MonthQuantityChart orders={orders} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    orders: state.order.orders,
  };
};

const mapDispathToProps = (dispath) => {
  return {
    fetchOrders: () => dispath(fetchOrders()),
  };
};

export default connect(mapStateToProps, mapDispathToProps)(StatisticDashboard);
