const initState = {};

const customerReducer = (state = initState, action) => {
  switch (action.type) {
    case "FETCH_CUSTOMERS":
      return {
        customers: action.customers.data,
      };
    case "CREATE_CUSTOMER":
      return {
        ...state,
        customers: [...state.customers, action.customers.data],
      };
    case "UPDATE_CUSTOMER":
      return {
        ...state,
        customers: state.customers.map((item) => (item.id === action.customers.data.id ? action.customers.data : item)),
      };
    case "DELETE_CUSTOMER":
      return {
        ...state,
        customers: state.customers.filter((item) => item.id !== action.customers.data.id),
      };
    case "SET_CURRENT_FIELD_CUSTOMER":
      return {
        ...state,
        curentFields: action.customer,
      };
    case "FETCH_CUSTOMER_DETAIL":
      return {
        ...state,
        customerDetail: action.customer.data,
      };
    default:
      return state;
  }
};

export default customerReducer;
