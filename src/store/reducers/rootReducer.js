import authReducer from "./authReducer";
import customerReducer from "./customerReducer";
import orderReducer from "./orderReducer";
import {combineReducers} from "redux";

const rootReducer = combineReducers({
  auth: authReducer,
  customer: customerReducer,
  order: orderReducer,
});

export default rootReducer;

// the key name will be the data property on the state object
