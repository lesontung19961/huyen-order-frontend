const authUserData = localStorage.getItem("authUserData");
const jsonAuthUserData = JSON.parse(authUserData);

const initState = {
  authUser: jsonAuthUserData,
};

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case "AUTH_LOGIN":
      return {
        authUser: action.payload.data,
      };
    case "AUTH_SIGNUP":
      return {
        authUser: action.payload.data,
      };
    case "AUTH_LOGOUT":
      return {
        authUser: null,
      };
    default:
      return state;
  }
};

export default authReducer;
