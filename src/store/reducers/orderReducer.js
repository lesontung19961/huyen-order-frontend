const initState = {};

const orderReducer = (state = initState, action) => {
  switch (action.type) {
    case "FETCH_ORDERS":
      return {
        orders: action.orders.data,
      };
    case "CREATE_ORDER":
      return {
        ...state,
        orders: [...state.orders, action.orders.data],
      };
    case "UPDATE_ORDER":
      return {
        ...state,
        orders: state.orders.map((item) => (item.id === action.orders.data.id ? action.orders.data : item)),
      };
    case "DELETE_ORDER":
      return {
        ...state,
        orders: state.orders.filter((item) => item.id !== action.orders.data.id),
      };
    case "SET_CURRENT_FIELD_ORDER":
      return {
        ...state,
        curentFields: action.order,
      };
    case "SET_STATUS_FILTER":
      return {
        ...state,
        statusFilter: action.payload,
      };
    default:
      return state;
  }
};

export default orderReducer;
