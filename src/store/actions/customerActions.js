import axios from "axios";
import {apiHomeUrl} from "../../config/main";

export const fetchCustomers = () => {
  return (dispatch, state) => {
    // handle success
    axios.get(`${apiHomeUrl}/customers?_limit=-1`).then((response) => {
      dispatch({
        type: "FETCH_CUSTOMERS",
        customers: response,
      });
    });
  };
};

export const fetchCustomerDetail = (id) => {
  return (dispatch, state) => {
    // handle success
    axios.get(`${apiHomeUrl}/customers/${id}`).then((response) => {
      dispatch({
        type: "FETCH_CUSTOMER_DETAIL",
        customer: response,
      });
    });
  };
};

export const createCustomer = (customer) => {
  return (dispatch, state) => {
    return new Promise((resolve, reject) => {
      // handle success
      axios.post(`${apiHomeUrl}/customers`, customer).then((response) => {
        dispatch({
          type: "CREATE_CUSTOMER",
          customers: response,
        });
        resolve(response.data);
      });
    });
  };
};

export const updateCustomer = (customer) => {
  return (dispatch, state) => {
    return new Promise((resolve, reject) => {
      axios.put(`${apiHomeUrl}/customers/${customer.id}`, customer).then((response) => {
        dispatch({
          type: "UPDATE_CUSTOMER",
          customers: response,
        });
        resolve();
      });
    });
  };
};

export const deleteCustomer = (customer) => {
  return (dispatch, state) => {
    return new Promise((resolve, reject) => {
      axios.delete(`${apiHomeUrl}/customers/${customer.id}`).then((response) => {
        dispatch({
          type: "DELETE_CUSTOMER",
          customers: response,
        });
        resolve();
      });
    });
  };
};

export const setCurrentFieldCustomer = (customer) => {
  return (dispatch, state) => {
    // handle success
    dispatch({
      type: "SET_CURRENT_FIELD_CUSTOMER",
      customer: customer,
    });
  };
};
