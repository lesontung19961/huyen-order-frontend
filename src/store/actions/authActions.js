import axios from "axios";
import {apiHomeUrl} from "../../config/main";

export const authLogin = (body) => {
  return (dispatch, state) => {
    return new Promise((resolve, reject) => {
      axios
        .post(`${apiHomeUrl}/auth/local`, body)
        .then((response) => {
          dispatch({
            type: "AUTH_LOGIN",
            payload: response,
          });
          resolve(response.data);
        })
        .catch((error) => {
          reject();
        });
    });
  };
};

export const authSignUp = (body) => {
  return (dispatch, state) => {
    return new Promise((resolve, reject) => {
      axios
        .post(`${apiHomeUrl}/auth/local/register`, body)
        .then((response) => {
          dispatch({
            type: "AUTH_SIGNUP",
            payload: response,
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.log("error", error);
          reject(error);
        });
    });
  };
};

export const authLogout = () => {
  return (dispatch, state) => {
    dispatch({
      type: "AUTH_LOGOUT",
      payload: {},
    });
  };
};
