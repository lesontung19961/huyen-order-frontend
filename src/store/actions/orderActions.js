import axios from "axios";
import {apiHomeUrl} from "../../config/main";

export const fetchOrders = () => {
  return (dispatch, state) => {
    // handle success
    axios.get(`${apiHomeUrl}/orders?_sort=date,id&_limit=-1`).then((response) => {
      dispatch({
        type: "FETCH_ORDERS",
        orders: response,
      });
    });
  };
};

export const createOrder = (order) => {
  return (dispatch, state) => {
    return new Promise((resolve, reject) => {
      axios.post(`${apiHomeUrl}/orders`, order).then((response) => {
        dispatch({
          type: "CREATE_ORDER",
          orders: response,
        });
        resolve();
      });
    });
  };
};

export const updateOrder = (order) => {
  return (dispatch, state) => {
    return new Promise((resolve, reject) => {
      axios.put(`${apiHomeUrl}/orders/${order.id}`, order).then((response) => {
        dispatch({
          type: "UPDATE_ORDER",
          orders: response,
        });
        resolve();
      });
    });
  };
};

export const deleteOrder = (order) => {
  return (dispatch, state) => {
    return new Promise((resolve, reject) => {
      axios.delete(`${apiHomeUrl}/orders/${order.id}`).then((response) => {
        dispatch({
          type: "DELETE_ORDER",
          orders: response,
        });
        resolve();
      });
    });
  };
};

export const setCurrentFieldOrder = (order) => {
  return (dispatch, state) => {
    // handle success
    dispatch({
      type: "SET_CURRENT_FIELD_ORDER",
      order: order,
    });
  };
};

export const setFilterStatusOrder = (status) => {
  return (dispatch, state) => {
    dispatch({
      type: "SET_STATUS_FILTER",
      payload: status,
    });
  };
};
