import React from "react";
import axios from "axios";
import {BrowserRouter, Switch, Route} from "react-router-dom";

import SignIn from "./components/auth/SignIn";
import SignUp from "./components/auth/SignUp";
import LogOut from "./components/auth/LogOut";
import OrderDashboard from "./components/order/OrderDashboard";
import CustomerDashboard from "./components/customer/CustomerDashboard";
import CustomerDetail from "./components/customer/CustomerDetail";
import Navbar from "./components/layout/Navbar";
import StatisticDashboard from "./components/statistic/StatisticDashboard";
import ChartTest from "./components/statistic/ChartTest";

function App() {
  const authUserData = localStorage.getItem("authUserData");
  if (authUserData) {
    const {jwt} = JSON.parse(authUserData);
    axios.defaults.headers.common["Authorization"] = `Bearer ${jwt}`;
  } else {
    delete axios.defaults.headers.common["Authorization"];
  }
  return (
    <BrowserRouter>
      <Navbar />
      <div className="App">
        <Switch>
          <Route exact path="/" component={OrderDashboard} />
          <Route exact path="/customer" component={CustomerDashboard} />
          <Route exact path="/customer/:id" component={CustomerDetail} />
          <Route exact path="/statistic" component={StatisticDashboard} />
          <Route exact path="/charttest" component={ChartTest} />
          <Route path="/signin" component={SignIn} />
          <Route path="/signup" component={SignUp} />
          <Route path="/logout" component={LogOut} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
